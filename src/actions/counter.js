import {
  UPDATE_COUNTER, SET_COUNTER, QUIT_COUNTER, LOAD_COUNTER, INIT_COUNTER
} from './types/counter'

const quitCounter = () => {
  return { type: QUIT_COUNTER };
}

const updateCounter = (amount) => {
  return { type: UPDATE_COUNTER, payload: amount };
}

const setCounter = (amount) => {
  return { type: SET_COUNTER, payload: amount };
}

const loadCounter = () => {
  return { type: LOAD_COUNTER}
}

const initCounter = (counter) => {
  return { type: INIT_COUNTER, payload: counter };
}

export { updateCounter, setCounter, quitCounter, loadCounter, initCounter }
