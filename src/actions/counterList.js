import { SHOW_COUNTERS, LOAD_COUNTERS } from './types/counterList'

const showCounters = (counters) => {
  return { type: SHOW_COUNTERS, payload: counters };
}

const loadCounters = () => {
  return { type: LOAD_COUNTERS };
}

export { showCounters, loadCounters }
