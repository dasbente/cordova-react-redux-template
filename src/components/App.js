import { connect } from 'react-redux'
import { updateCounter, setCounter } from '../actions/counter'
import Counter from '../containers/Counter'

const mapStateToProps = (state) => {
    return {count: state.count};
}

const mapDispatchToProps = dispatch => {
  return {
    increment: () => {dispatch(updateCounter(1))},
    reset: () => {dispatch(setCounter(0))},
    decrement: () => {dispatch(updateCounter(-1))},
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Counter)
