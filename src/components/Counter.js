import React from 'react'
import {connect} from 'react-redux'

import Counter from '../containers/Counter'

import { setCounter, updateCounter, quitCounter } from '../actions/counter'

const mapStateToProps = state => {
  return {
    amount: state.counter.counter.amount,
    title: state.counter.counter.title,
    code: state.counter.counter.code,
  };
}

const mapDispatchToProps = dispatch => {
  return {
    inc: () => dispatch(updateCounter(1)),
    dec: () => dispatch(updateCounter(-1)),
    reset: () => dispatch(setCounter(0)),
    quit: () => dispatch(quitCounter()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Counter);
