import React from 'react'
import $ from 'jquery'
import { connect } from 'react-redux';

import { showCounters, loadCounters } from '../actions/counterList'

const CounterInfo = ({title, amount}) => {
  return(
    <div className="my-1 row mb-md-2">
      <p className="col h4 text-right">{title}</p>
      <p className="col-1 h4 text-muted">{amount}</p>
    </div>
  );
}

const CounterList = ({counters, loading, onClick}) => {
  return (
    <div className="mt-4">
      <button className="btn btn-link" onClick={onClick} disabled={loading}>List Counters</button>
      <div>
        {counters.map(
          (c, i) => (<CounterInfo key={i} title={c.title} amount={c.amount} />)
        )}
      </div>
    </div>
  )
}

const mapStateToProps = ({counters}) => {
  return {
    counters: counters.list,
    loading: counters.loading,
  }
}

const refreshList = (dispatch) => {
  dispatch(loadCounters());
  $.get('/counter/all').then(
    (counters) => dispatch(showCounters(JSON.parse(counters)))
  );
}

const mapDispatchToProps = dispatch => {
  return {
    onClick: () => refreshList(dispatch)
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CounterList)

export { refreshList }
