import React from 'react'

import { connect } from 'react-redux'

import { Link } from 'react-router-dom'

import NewCounterForm from '../containers/NewCounterForm'
import FindCounterForm from '../containers/FindCounterForm'
import Counter from '../components/Counter'
import CounterList, { refreshList } from '../components/CounterList'
import NukeDB from '../containers/NukeDB'

import { quitCounter, initCounter } from '../actions/counter'


const Index = ({counterSelected, quit, find, nuke}) => {
  return (
    <div>
      { counterSelected? (<Counter onQuit={quit}/>) : (<FindCounterForm onSubmit={find}/>) }
      <div className="mt-4">
        <button className="btn btn-link" data-toggle="collapse" href="#newCounter" role="button" aria-expanded="false"
           aria-controls="newCounter">New Counter</button>
        <NewCounterForm />
      </div>
      <CounterList />
      <NukeDB onDelete={nuke}/>
    </div>
  );
}

const mapStateToProps = state => {
  return {counterSelected: state.counter.active};
};

const mapDispatchToProps = dispatch => {
  return {
    quit: () => dispatch(quitCounter),
    find: (counter) => dispatch(initCounter(counter)),
    nuke: () => refreshList(dispatch)
  };
};

export default connect(
  mapStateToProps, mapDispatchToProps
)(Index)
