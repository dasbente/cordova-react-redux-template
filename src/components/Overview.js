import { connect } from 'react-redux'
import React from 'react'
import { Link } from 'react-router-dom'

const CounterInfo = ({counter, ind}) => {
  return (
    <div className="row" key={ind}>
      <p>{counter.title}</p>
      <p>{counter.amount}</p>
    </div>
  );
}

const Overview = ({counters}) => {
  return (
    <div>
      <Link className="btn btn-primary" to="/">Back</Link>
      {counters.map((counter, i) => {
        return <CounterInfo counter={counter} ind={i}/>
      })}
    </div>
  );
}

const mapStateToProps = state => {
  return {
    counters: state.counters
  }
}
const mapDispatchToProps = dispatch => {
  return {}
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Overview);
