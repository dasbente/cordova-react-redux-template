import React from 'react'

const Button = ({onClick, children, className}) => {
  return (
    <button type="button" className={"btn btn-outline-primary " + className}
            onClick={onClick}>
      {children}
    </button>
  );
}

export default Button
