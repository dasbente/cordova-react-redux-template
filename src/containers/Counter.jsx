import React from 'react'
import $ from 'jquery'

import Button from './Button'

const onQuit = (code, amount, callback) => {
  $.post(`/counter/update/${code}/${amount}/`).then(() => callback());
}

const onDelete = (code, callback) => {
  $.post(`/counter/delete/${code}/`).then(() => callback());
}

const Counter = ({amount, code, title, inc, dec, reset, quit}) => {
  return (
    <div className="mt-4">
      <h4>{title}</h4><br />
      <div className="row">
        <div className="input-group col-md-6">
          <div className="input-group-prepend">
            <Button onClick={dec}>-</Button>
          </div>
          <input type="text" className="form-control" value={amount} readOnly></input>
          <div className="input-group-append">
            <Button onClick={inc}>+</Button>
          </div>
        </div>

        <Button onClick={reset} className="col">Reset</Button>
        <Button onClick={() => onQuit(code, amount, quit)} className="col mx-2">Quit</Button>
        <button onClick={() => onDelete(code, quit)} className="btn btn-danger col mx-4">
          Delete
        </button>
      </div>
    </div>
  );
}

export default Counter
