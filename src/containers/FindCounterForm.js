import React from 'react'
import $ from 'jquery'

class FindCounterForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {code: ''};
    this.codeInput = this.codeInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  codeInput(e) {
    this.setState({code: e.target.value});
  }

  handleSubmit() {
    $.get(`/counter/find/${this.state.code}/`,
      counter => this.props.onSubmit(JSON.parse(counter))
    );
    this.setState({code: ''});
  }

  render() {
    return (
      <form className="input-group mt-4 w-100">
        <input type="password" className="form-control" aria-label="Passcode Input"
               placeholder="Code" value={this.state.code} onChange={this.codeInput}/>
        <div className="input-group-append">
          <button className="btn btn-outline-primary" onClick={this.handleSubmit} type="button">
            Go!
          </button>
        </div>
      </form>
    );
  }
}

export default FindCounterForm;
