import React from 'react'
import $ from 'jquery'

class NewCounter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {title: '', code: ''};
    this.newTitle =  this.newTitle.bind(this)
    this.newCode = this.newCode.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  newTitle(e) {
    this.setState({title: e.target.value})
  }

  newCode(e) {
    this.setState({code: e.target.value})
  }

  handleSubmit() {
    $.post(`/counter/new/${this.state.code}/${this.state.title}/`).then(
      (_) => this.props.onSubmit({code: code, title: title, amount: 0})
    );
    this.setState({title: '', code: ''});
  }

  render() {
    return (
      <form className="collapse" id="newCounter">
        <input type="text" className="mt-1 mr-2 w-100 col" placeholder="Title"
               value={this.state.title} onChange={this.newTitle}/>
        <input type="password" className="mt-1 mr-2 w-100 col" placeholder="Code"
               value={this.state.code} onChange={this.newCode}/>
        <button className="btn btn-primary mt-1 float-right" onClick={this.handleSubmit}
                type="button">Create</button>
      </form>
    );
  }
}

export default NewCounter
