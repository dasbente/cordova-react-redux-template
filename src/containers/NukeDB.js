import React from 'react'
import $ from 'jquery'

class NukeDB extends React.Component {
  constructor(props) {
    super(props);
    this.state = { password: '' };

    this.handleNuke = this.handleNuke.bind(this);
    this.onPasswordChange = this.onPasswordChange.bind(this);
  }

  onPasswordChange(e) {
    this.setState({password: e.target.value});
  }

  handleNuke() {
    $.post(`/counter/nuke/${this.state.password}/`).then(this.props.onDelete)
    this.setState({ password: '' });
  }

  render() {
    return (
      <div className="input-group col-md-6 float-right">
        <input type="password" className="form-control" onChange={this.onPasswordChange}/>
        <div className="input-group-append">
          <button className="btn btn-danger" onClick={this.handleNuke}>Nuke database?</button>
        </div>
      </div>
    );
  }
}

export default NukeDB
