import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux'
import { createStore } from 'redux'
import App from './components/App'
import rootReducer from './reducers'

import Index from './components/Index'

const store = createStore(rootReducer);

const rootElement = document.getElementById('app');

ReactDOM.render(
  <Provider store={store}>
    <div className="container">
      <Index />
    </div>
  </Provider>,
  rootElement
);
