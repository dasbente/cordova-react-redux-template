import { combineReducers } from 'redux'

import {
  UPDATE_COUNTER, SET_COUNTER, LOAD_COUNTER, QUIT_COUNTER, INIT_COUNTER
} from '../actions/types/counter'

const initialState = { counter: {title: '', amount: 0}, active: false, loading: false}

const loading = (state = initialState.loading, action) => {
  switch (action.type) {
    case LOAD_COUNTER:
      return true;
    case INIT_COUNTER:
      return false
    default:
      return state;
  }
}

const active = (state = initialState.active, action) => {
  switch (action.type) {
    case INIT_COUNTER:
      return true;
    case QUIT_COUNTER:
      return false;
    default:
      return state;
  }
}

const counter = (state = initialState.counter, action) => {
  switch (action.type) {
    case INIT_COUNTER:
      let {title, amount, code} = action.payload
      return {title, amount, code};
    case SET_COUNTER:
      return Object.assign({}, state, {amount: action.payload});
    case UPDATE_COUNTER:
      return Object.assign({}, state, {amount: state.amount + action.payload});
    default:
      return state;
  }
}

export default combineReducers({
  counter, loading, active
})
