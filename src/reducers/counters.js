import { LOAD_COUNTERS, SHOW_COUNTERS } from '../actions/types/counterList'

const initialState = { list: [], loading: false }

const counters = (state = initialState, {type, payload}) => {
  switch (type) {
    case LOAD_COUNTERS:
      return Object.assign({}, state, { loading: true })
    case SHOW_COUNTERS:
      return { loading: false, list: payload };
    default:
      return state
  }
  return state
}

export default counters
