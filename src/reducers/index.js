import { combineReducers } from 'redux'
import counter from './counter'
import counters from './counters'

export default combineReducers({counters, counter});
