const express = require('express')
const app = express()
const path = require('path')

const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const PASSWORD = '1234';

app.use('/dist', express.static(path.join(__dirname, 'dist')));
app.use(bodyParser.urlencoded({extended: false}));

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/index.html'))
});

const handleError = (err) => {
  res.send(JSON.stringify(err));
}

mongoose.connect(
  'mongodb://mongo:27017/counterapp',
  { useNewUrlParser: true }
).then(
  () => console.log('MongoDB Connected')
).catch(err => console.log(err));

const CounterSchema = new mongoose.Schema({
  title: { type: String, default: '' },
  code: { type: String, required: true },
  amount: { type: Number, default: 0 }
});

/*
CounterSchema.methods.toJSON = () => {
  // Dont expose code property to the outside world
  var obj = this.toObject();
  return {title: obj.title, amount: obj.amount};
}
*/
const Counter = mongoose.model('counter', CounterSchema);

const findCounter = (code, callback) => {
  Counter.findOne({code: code}).exec(
    (err, res) => err ? handleError(err) : callback(res)
  )
}

const allCounters = (callback) => {
  Counter.find({}).exec(
    (err, res) => err ? handleError(err) : callback(res)
  )
}

const newCounter = (code, title, callback) => {
  const counter = new Counter({ title: title, code: code });
  counter.save().then(callback);
}

const updateCounter = (code, amount, callback) => {
  const counter = Counter.findOneAndUpdate({ code: code }, {$set: {amount: amount}},
    (err, res) => err? handleError(err) : callback(res)
  )
}

const deleteByCode = (code, callback) => {
  Counter.find({code: code}).remove().then(callback);
}

const nukeSchema = (callback) => {
  Counter.find({}).remove().then(callback);
}

app.post('/counter/new/:code/:title', (req, res) => {
  newCounter(req.params.code, req.params.title, item => res.redirect('/'));
});

app.get('/counter/all', (req, res) => {
  allCounters( (list) => res.send(JSON.stringify(list)))
});

app.get('/counter/find/:code', (req, res) => {
  findCounter(req.params.code, (counter) => res.send(JSON.stringify(counter)));
})

app.post('/counter/update/:code/:amount', (req, res) => {
  updateCounter(req.params.code, req.params.amount, () => res.redirect('/'));
})

app.post('/counter/delete/:code/', (req, res) => {
  deleteByCode(req.params.code, () => res.redirect('/'));
})

app.post('/counter/nuke/:passcode/', (req, res) => {
  if (req.params.passcode == PASSWORD) {
    nukeSchema(() => res.redirect('/'));
  }
})

const port = 8000
const host = '0.0.0.0'

app.listen(port, host,
  () => console.log(`Example app listening on port ${port}!`)
);
